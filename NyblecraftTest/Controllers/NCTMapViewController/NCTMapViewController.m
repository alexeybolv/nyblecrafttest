//
//  NCTMapViewController.m
//  NyblecraftTest
//
//  Created by Алексей on 08.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTMapViewController.h"

//Managers
#import "NCTAPIManager.h"

//Models
#import "NCTCurrentWeatherRequest+CoreDataClass.h"

//Libs
#import <GoogleMaps/GoogleMaps.h>
#import <MagicalRecord/MagicalRecord.h>
#import <SVProgressHUD/SVProgressHUD.h>

//Controllers
#import "NCTCurrentWeatherViewController.h"

@interface NCTMapViewController () <CLLocationManagerDelegate, GMSMapViewDelegate, UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *markerImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) CLLocationCoordinate2D currentCoordinate;
@property (strong, nonatomic) NCTCurrentWeatherRequest *currentWeatherRequest;

@end

static const NSInteger NCTMapViewControllerAddressDefaultZoom = 14;

@implementation NCTMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLocationManager];
    [self setUpMap];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SetUps

- (void)setUpLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locationManager.distanceFilter = 300;
    [self.locationManager requestWhenInUseAuthorization];
}

- (void)setUpMap {
    self.mapView.delegate = self;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;
    self.mapView.camera = [GMSCameraPosition cameraWithLatitude:53.9
                                                      longitude:27.56667
                                                           zoom:NCTMapViewControllerAddressDefaultZoom];
    self.markerImageView.layer.zPosition = self.mapView.layer.zPosition + 1;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (locations.firstObject) {
        CLLocation *location = [locations lastObject];
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:NCTMapViewControllerAddressDefaultZoom];
        [manager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self.locationManager startUpdatingLocation];
        }
            break;
        default:
            break;
    }
}

#pragma mark - GoogleMaps

- (void)reverseGeocodeCoordinate:(CLLocationCoordinate2D) coordinate {
    self.latitudeLabel.text = [NSString stringWithFormat:@"lat: %f",coordinate.latitude];
    self.longitudeLabel.text = [NSString stringWithFormat:@"lon: %f",coordinate.longitude];
    self.currentCoordinate = coordinate;
    [[GMSGeocoder geocoder]reverseGeocodeCoordinate:coordinate
                                  completionHandler:^(GMSReverseGeocodeResponse * response, NSError *error){
                                      self.addressLabel.text = response.firstResult.thoroughfare;
                                      self.cityLabel.text = response.firstResult.locality;
                                  }];
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    [self reverseGeocodeCoordinate:position.target];
}

#pragma mark - UIPopoverPresentationControllerDelegate

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

#pragma mark - Actions 

- (IBAction)currentLocationBarButtonAction:(UIBarButtonItem *)sender {
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
}

- (IBAction)askWeatherAction:(id)sender {
    self.mapView.userInteractionEnabled = NO;

    [[NCTAPIManager sharedInstance] getCurrentWeatherWithLatitude:@(self.currentCoordinate.latitude) longitude:@(self.currentCoordinate.longitude) andCompletion:^(NSDictionary *data, NSString *error) {
        if (data) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                self.currentWeatherRequest = [NCTCurrentWeatherRequest createCurrentWeatherRequestWithDictionary:data address:self.addressLabel.text city:self.cityLabel.text latitude:self.currentCoordinate.latitude longitude:self.currentCoordinate.longitude inContext:localContext];
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                self.mapView.userInteractionEnabled = YES;
                self.currentWeatherRequest = [[NCTCurrentWeatherRequest MR_findAll] lastObject];
                NCTCurrentWeatherViewController *currentWeatherViewController = [NCTCurrentWeatherViewController storyboardInstance];
                currentWeatherViewController.currentWeatherRequest = self.currentWeatherRequest;
                currentWeatherViewController.modalPresentationStyle = UIModalPresentationPopover;
                CGFloat widhtOfVC = CGRectGetWidth(self.view.frame)/1.5;
                CGFloat heightOfVC = CGRectGetHeight(self.view.frame)/2.7;
                currentWeatherViewController.preferredContentSize = CGSizeMake(widhtOfVC, heightOfVC);
                UIPopoverPresentationController *presentationController = [currentWeatherViewController popoverPresentationController];
                presentationController.delegate = self;
                presentationController.sourceView = self.view;
                presentationController.sourceRect = CGRectMake((CGRectGetWidth(self.view.bounds) - widhtOfVC) / 2,
                                                               (CGRectGetHeight(self.view.bounds) - heightOfVC) / 2,
                                                               widhtOfVC,
                                                               heightOfVC);
                presentationController.permittedArrowDirections = UIMenuControllerArrowDefault;
                [self presentViewController:currentWeatherViewController animated:YES completion:nil];
            }];
        }
        else {
            self.mapView.userInteractionEnabled = YES;
            [SVProgressHUD showErrorWithStatus:error];
        }
    }];
}

@end
