//
//  NCTWeatherForDayViewController.h
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCTWeatherForDayViewController : UIViewController

@property (assign, nonatomic) float currentLatitude;
@property (assign, nonatomic) float currentLongitude;

+ (instancetype)storyboardInstance;

@end
