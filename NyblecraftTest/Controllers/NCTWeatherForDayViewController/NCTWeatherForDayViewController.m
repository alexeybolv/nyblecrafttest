//
//  NCTWeatherForDayViewController.m
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTWeatherForDayViewController.h"

//Managers
#import "NCTAPIManager.h"

//Models
#import "NCTWeatherForDay.h"

//Libs
#import <Charts/Charts-Swift.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface NCTWeatherForDayViewController () <ChartViewDelegate>

@property (strong, nonatomic) NSMutableArray <NCTWeatherForDay *> *weatherForDayArray;
@property (weak, nonatomic) IBOutlet LineChartView *lineChartView;

@end

@implementation NCTWeatherForDayViewController

#pragma mark - Getters

- (NSMutableArray *)weatherForDayArray {
    if (!_weatherForDayArray) _weatherForDayArray = [NSMutableArray array];
    return _weatherForDayArray;
}

#pragma mark - LifeCycle

+ (instancetype)storyboardInstance {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:self.description bundle:nil];
    return storyboard.instantiateInitialViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getWeatherForToday];
    self.lineChartView.delegate = self;
    self.lineChartView.descriptionText = @"Tap node for details";
    self.lineChartView.descriptionTextColor = [UIColor orangeColor];
    self.lineChartView.noDataText = @"No data provided";
}

#pragma mark - Get Data

- (void)getWeatherForToday {
    [[NCTAPIManager sharedInstance] getWeatherForDayWithLatitude:@(self.currentLatitude) longitude:@(self.currentLongitude) andCompletion:^(NSDictionary *data, NSString *error) {
        if (data) {
            for (id dict in data[@"list"]) {
                [self.weatherForDayArray addObject:[NCTWeatherForDay createWeatherForDayWithDictionary:dict]];
            }
            [self createChart];
        } else {
            [SVProgressHUD showErrorWithStatus:error];
        }
    }];
}

#pragma mark - Create Chart

- (void)createChart {
    
    NSMutableArray *xValsArray = [NSMutableArray array];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"E, d MMM HH:mm"];
    for (NCTWeatherForDay *weatherForDay in self.weatherForDayArray) {
        [xValsArray addObject:[dateFormatter  stringFromDate:weatherForDay.date]];
    }
    [self setChartDataWithXValsArray:[xValsArray copy]];
}

- (void)setChartDataWithXValsArray:(NSArray *)xVals {
    
    NSMutableArray *dataEntriesArray = [NSMutableArray array];
    for (int i = 0; i < xVals.count; i++) {
        [dataEntriesArray addObject:[[ChartDataEntry alloc] initWithValue:[self.weatherForDayArray[i].temp doubleValue] xIndex:i]];
    }
    
    LineChartDataSet *dataSet = [[LineChartDataSet alloc] initWithYVals:[dataEntriesArray copy] label:@"Temperature"];
    [dataSet setAxisDependency:AxisDependencyLeft];
    [dataSet setColor:[UIColor darkGrayColor] alpha:1];
    dataSet.circleColors = @[[UIColor darkGrayColor]];
    dataSet.lineWidth = 2.0;
    dataSet.circleRadius = 6.0; // the radius of the node circle
    dataSet.fillAlpha = 65 / 255.0;
    dataSet.fillColor = [UIColor orangeColor];
    dataSet.highlightColor = [UIColor orangeColor];
    dataSet.drawCircleHoleEnabled = YES;
    dataSet.valueFont = [UIFont systemFontOfSize:12];
    
    NSMutableArray *dataSetsArray = [NSMutableArray array];
    [dataSetsArray addObject:dataSet];
    
    LineChartData *lineChartData = [[LineChartData alloc] initWithXVals:xVals dataSets:dataSetsArray];
    [lineChartData setValueTextColor:[UIColor orangeColor]];
    
    self.lineChartView.data = lineChartData;
}


@end
