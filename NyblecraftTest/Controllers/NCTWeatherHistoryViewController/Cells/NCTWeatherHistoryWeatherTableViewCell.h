//
//  NCTWeatherHistoryWeatherTableViewCell.h
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCTWeatherHistoryWeatherTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;


@end
