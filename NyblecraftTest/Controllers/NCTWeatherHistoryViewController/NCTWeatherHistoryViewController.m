//
//  NCTWeatherHistoryViewController.m
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTWeatherHistoryViewController.h"

//Views
#import "NCTWeatherHistoryInfoTableViewCell.h"
#import "NCTWeatherHistoryWeatherTableViewCell.h"

//Controllers
#import "NCTWeatherForDayViewController.h"

//Models
#import "NCTCurrentWeatherRequest+CoreDataClass.h"

//Libs
#import <MagicalRecord/MagicalRecord.h>

@interface NCTWeatherHistoryViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray <NCTCurrentWeatherRequest *> *currentWeatherRequestArray;

@end

@implementation NCTWeatherHistoryViewController

#pragma mark - Getters

- (NSArray *)currentWeatherRequestArray {
    if (!_currentWeatherRequestArray) _currentWeatherRequestArray = [NSArray array];
    return _currentWeatherRequestArray;
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    self.currentWeatherRequestArray = [NCTCurrentWeatherRequest MR_findAll];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.currentWeatherRequestArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currentWeatherRequestArray[section].isOpened ? 2 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            NCTWeatherHistoryInfoTableViewCell *infoCell = [self.tableView dequeueReusableCellWithIdentifier:@"NCTWeatherHistoryInfoTableViewCell" forIndexPath:indexPath];
            [self setUpInfoCell:infoCell withCurrentWeatherRequest:self.currentWeatherRequestArray[indexPath.section]];
            return infoCell;
            break;
        }
        case 1: {
            NCTWeatherHistoryWeatherTableViewCell *weatherCell = [self.tableView dequeueReusableCellWithIdentifier:@"NCTWeatherHistoryWeatherTableViewCell" forIndexPath:indexPath];
            [self setUpWeatherCell:weatherCell withCurrentWeatherRequest:self.currentWeatherRequestArray[indexPath.section]];
            return weatherCell;
            break;
        }
    }
    return nil;
}

#pragma mark - SetUp Cells

- (void)setUpInfoCell:(NCTWeatherHistoryInfoTableViewCell *)infoCell withCurrentWeatherRequest:(NCTCurrentWeatherRequest *)currentWeatherRequest {
    infoCell.cityLabel.text = currentWeatherRequest.city;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, H:mm a"];
    infoCell.dateLabel.text = [dateFormatter stringFromDate:currentWeatherRequest.date];
    infoCell.latitudeLabel.text = [NSString stringWithFormat:@"lat: %f",currentWeatherRequest.latitude ];
    infoCell.longitudeLabel.text = [NSString stringWithFormat:@"lon: %f",currentWeatherRequest.longitude ];
    if (currentWeatherRequest.isOpened) {
        infoCell.arrowImageView.image = [UIImage imageNamed:@"arrowUp"];
        infoCell.separatorInset = UIEdgeInsetsMake(0.f, infoCell.bounds.size.width, 0.f, 0.f);
    } else {
        infoCell.arrowImageView.image = [UIImage imageNamed:@"arrowDown"];
        infoCell.separatorInset = UIEdgeInsetsMake(0.f, 15.0f, 0.f, 15.0f);
    }
    infoCell.arrowImageView.tintColor = [UIColor orangeColor];
}

- (void)setUpWeatherCell:(NCTWeatherHistoryWeatherTableViewCell *)weatherCell withCurrentWeatherRequest:(NCTCurrentWeatherRequest *)currentWeatherRequest {
    weatherCell.temperatureLabel.text = [NSString stringWithFormat:@"%d °C",(int)currentWeatherRequest.weatherTemp];
    weatherCell.mainLabel.text = currentWeatherRequest.weatherMain;
    weatherCell.windLabel.text = [NSString stringWithFormat:@"wind : %@ m/s",currentWeatherRequest.windSpeed];
    weatherCell.pressureLabel.text = [NSString stringWithFormat:@"pressure : %@ mbar",currentWeatherRequest.pressure];
}

#pragma mark - UITableViewDelegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NCTCurrentWeatherRequest *currentWeatherRequest = self.currentWeatherRequestArray[indexPath.section];
    currentWeatherRequest.opened = !currentWeatherRequest.isOpened;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            return 91;
            break;
        }
        case 1: {
            return 134;
            break;
        }
    }
    return 0;
}

#pragma mark - Actions 

- (IBAction)weatherForTodayAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NCTCurrentWeatherRequest *currentWeatherRequest = self.currentWeatherRequestArray[indexPath.section];
    NCTWeatherForDayViewController *weatherForDayViewController = [NCTWeatherForDayViewController storyboardInstance];
    weatherForDayViewController.currentLongitude = currentWeatherRequest.longitude;
    weatherForDayViewController.currentLatitude = currentWeatherRequest.latitude;
    [self.navigationController pushViewController:weatherForDayViewController animated:YES];
}


@end
