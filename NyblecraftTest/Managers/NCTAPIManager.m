//
//  NCTAPIManager.m
//  NyblecraftTest
//
//  Created by Алексей on 09.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTAPIManager.h"

//Libs
#import <AFNetworking/AFNetworking.h>

static NSString * const NCTAPIManagerBaseURL = @"http://api.openweathermap.org/";
static NSString * const NCTAppID = @"96f6c15f795066aaabedaea09b627a6f";

@interface NCTAPIManager ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation NCTAPIManager

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:NCTAPIManagerBaseURL] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (void)getCurrentWeatherWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andCompletion:(void(^)(NSDictionary *data, NSString *error))completion {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *requestString = [NSString stringWithFormat:@"data/2.5/weather?lat=%@&lon=%@&APPID=%@",latitude,longitude,NCTAppID];
    self.lastTask = [self.sessionManager GET:requestString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.lastTask = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (responseObject) {
            if (completion) {
                NSDictionary *data = responseObject;
                completion(data, nil);
            }
        } else {
            if (completion) {
                completion(nil,nil);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.lastTask = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (completion) {
            completion(nil,error.localizedDescription);
        }
    }];
}


- (void)getWeatherForDayWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andCompletion:(void(^)(NSDictionary *data, NSString *error))completion {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *requestString = [NSString stringWithFormat:@"data/2.5/forecast?lat=%@&lon=%@&APPID=%@",latitude,longitude,NCTAppID];
    self.lastTask = [self.sessionManager GET:requestString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.lastTask = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (responseObject) {
            if (completion) {
                NSDictionary *data = responseObject;
                completion(data, nil);
            }
        } else {
            if (completion) {
                completion(nil,nil);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.lastTask = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (completion) {
            completion(nil,error.localizedDescription);
        }
    }];
}


//- (void)getWeatherWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andCompletion:(void(^)(NSDictionary *data, NSString *error))completion {
//    
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    NSString *requestString = [NSString stringWithFormat:@"data/2.5/forecast?lat=%@&lon=%@&APPID=%@",latitude,longitude,NCTAppID];
//    self.lastTask = [self.sessionManager GET:requestString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        self.lastTask = nil;
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        if (responseObject) {
//            if (completion) {
//                NSDictionary *data = responseObject;
//                completion(data, nil);
//            }
//        } else {
//            if (completion) {
//                completion(nil,nil);
//            }
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        self.lastTask = nil;
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        if (completion) {
//            completion(nil,error.localizedDescription);
//        }
//    }];
//}


@end
