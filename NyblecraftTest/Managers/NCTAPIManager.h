//
//  NCTAPIManager.h
//  NyblecraftTest
//
//  Created by Алексей on 09.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCTAPIManager : NSObject

@property (strong,nonatomic) NSURLSessionDataTask *lastTask;

+ (instancetype)sharedInstance;

- (void)getCurrentWeatherWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andCompletion:(void(^)(NSDictionary *data, NSString *error))completion;
- (void)getWeatherForDayWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andCompletion:(void(^)(NSDictionary *data, NSString *error))completion;

@end
