//
//  NCTWeatherForDay.h
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCTWeatherForDay : NSObject

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSNumber *temp;
@property (strong, nonatomic) NSString *weatherMain;
@property (strong, nonatomic) NSNumber *windSpeed;
@property (strong, nonatomic) NSNumber *pressure;

+ (NCTWeatherForDay *)createWeatherForDayWithDictionary:(NSDictionary *)dict;

@end
