//
//  NCTWeatherForDay.m
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTWeatherForDay.h"

@class Charts;

static const double NCTDifferentBetweenKelvinAndCelcius = 273.15;

@implementation NCTWeatherForDay

+ (NCTWeatherForDay *)createWeatherForDayWithDictionary:(NSDictionary *)dict {
    
    NCTWeatherForDay *weatherForDay = [[NCTWeatherForDay alloc] init];
    
    weatherForDay.temp = @([dict[@"main"][@"temp"] doubleValue] - NCTDifferentBetweenKelvinAndCelcius);
    weatherForDay.weatherMain = [dict[@"weather"] firstObject][@"main"];
    weatherForDay.pressure = dict[@"main"][@"pressure"];
    weatherForDay.windSpeed = dict[@"wind"][@"speed"];

    NSTimeInterval timeInterval = [dict[@"dt"] doubleValue];
    weatherForDay.date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    return weatherForDay;
}

@end
