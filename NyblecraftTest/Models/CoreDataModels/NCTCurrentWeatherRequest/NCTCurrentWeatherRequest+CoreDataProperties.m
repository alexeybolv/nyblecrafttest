//
//  NCTCurrentWeatherRequest+CoreDataProperties.m
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTCurrentWeatherRequest+CoreDataProperties.h"

@implementation NCTCurrentWeatherRequest (CoreDataProperties)

+ (NSFetchRequest<NCTCurrentWeatherRequest *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"NCTCurrentWeatherRequest"];
}

@dynamic address;
@dynamic city;
@dynamic date;
@dynamic latitude;
@dynamic longitude;
@dynamic weatherMain;
@dynamic weatherTemp;
@dynamic pressure;
@dynamic windSpeed;

@end
