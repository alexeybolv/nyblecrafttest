//
//  NCTCurrentWeatherRequest+CoreDataClass.m
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTCurrentWeatherRequest+CoreDataClass.h"

//Libs
#import <MagicalRecord/MagicalRecord.h>

static const double NCTDifferentBetweenKelvinAndCelcius = 273.15;

@implementation NCTCurrentWeatherRequest

@synthesize opened = _opened;

+ (NCTCurrentWeatherRequest *)createCurrentWeatherRequestWithDictionary:(NSDictionary *)dict address:(NSString *)address city:(NSString *)city latitude:(double)latitude longitude:(double)longitude inContext:(NSManagedObjectContext *)context {
    
    NCTCurrentWeatherRequest *currentWeatherRequest = [NCTCurrentWeatherRequest MR_createEntityInContext:context];
    currentWeatherRequest.weatherTemp = ([dict[@"main"][@"temp"] doubleValue] - NCTDifferentBetweenKelvinAndCelcius);
    currentWeatherRequest.weatherMain = [dict[@"weather"] firstObject][@"main"];
    currentWeatherRequest.pressure = dict[@"main"][@"pressure"];
    currentWeatherRequest.windSpeed = dict[@"wind"][@"speed"];
    currentWeatherRequest.city = city;
    currentWeatherRequest.address = address;
    currentWeatherRequest.latitude = latitude;
    currentWeatherRequest.longitude = longitude;
    currentWeatherRequest.date = [NSDate date];
    return currentWeatherRequest;
}

@end
