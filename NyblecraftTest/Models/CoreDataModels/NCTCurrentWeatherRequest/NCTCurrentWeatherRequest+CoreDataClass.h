//
//  NCTCurrentWeatherRequest+CoreDataClass.h
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CLLocation;

NS_ASSUME_NONNULL_BEGIN

@interface NCTCurrentWeatherRequest : NSManagedObject

@property (assign, nonatomic, getter=isOpened) BOOL opened;

+ (NCTCurrentWeatherRequest *)createCurrentWeatherRequestWithDictionary:(NSDictionary *)dict address:(NSString *)address city:(NSString *)city latitude:(double)latitude longitude:(double)longitude inContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "NCTCurrentWeatherRequest+CoreDataProperties.h"
