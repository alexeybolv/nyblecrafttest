//
//  NCTCurrentWeatherRequest+CoreDataProperties.h
//  NyblecraftTest
//
//  Created by Алексей on 10.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTCurrentWeatherRequest+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface NCTCurrentWeatherRequest (CoreDataProperties)

+ (NSFetchRequest<NCTCurrentWeatherRequest *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nullable, nonatomic, copy) NSString *weatherMain;
@property (nonatomic) double weatherTemp;
@property (nullable, nonatomic, copy) NSNumber *pressure;
@property (nullable, nonatomic, copy) NSNumber *windSpeed;

@end

NS_ASSUME_NONNULL_END
