//
//  NCTCurrentWeatherViewController.m
//  NyblecraftTest
//
//  Created by Алексей on 09.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NCTCurrentWeatherViewController.h"

//Models
#import "NCTCurrentWeatherRequest+CoreDataClass.h"

@interface NCTCurrentWeatherViewController ()

@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;

@end

@implementation NCTCurrentWeatherViewController

+ (instancetype)storyboardInstance {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:self.description bundle:nil];
    return storyboard.instantiateInitialViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.temperatureLabel.text = [NSString stringWithFormat:@"%d °C",(int)self.currentWeatherRequest.weatherTemp];
    self.mainLabel.text = self.currentWeatherRequest.weatherMain;
    self.windLabel.text = [NSString stringWithFormat:@"wind : %@ m/s",self.currentWeatherRequest.windSpeed];
    self.pressureLabel.text = [NSString stringWithFormat:@"pressure : %@ mbar",self.currentWeatherRequest.pressure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
