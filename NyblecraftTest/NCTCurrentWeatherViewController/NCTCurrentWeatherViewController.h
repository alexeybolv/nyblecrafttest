//
//  NCTCurrentWeatherViewController.h
//  NyblecraftTest
//
//  Created by Алексей on 09.11.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NCTCurrentWeatherRequest;

@interface NCTCurrentWeatherViewController : UIViewController

@property (strong, nonatomic) NCTCurrentWeatherRequest *currentWeatherRequest;

+ (instancetype)storyboardInstance;

@end
